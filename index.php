<?php

class Player
{

    public $name;
    public $coin;

    public function __construct($name, $coin)
    {
        $this->name = $name;
        $this->coin = $coin;
    }
}

class Game
{

    protected $player1;
    protected $player2;
    protected $flips = 1;

    /**
     * @return mixed
     */
    public function __construct($player1, $player2)
    {
        $this->player1 = $player1;
        $this->player2 = $player2;
    }

    public function start()
    {
        while (true) {
            $flip = rand(0, 1) ? "орел" : "решка";

            if ($flip == "орел") {
                $this->player1->coin++;
                $this->player2->coin--;
            } else {
                $this->player1->coin--;
                $this->player2->coin++;
            }

            if ($this->player1->coin == 0 || $this->player2->coin == 0) {
                return $this->end();
            }

            $this->flips++;
        }

    }

    public function winner()
    {
        if ($this->player1->coin > $this->player2->coin) {
            return $this->player1;
        } else {
            return $this->player2;
        }
    }

    public function end()
    {
        echo <<<EOT
        
            Game over.
            
            {$this->player1->name} : {$this->player1->coin}
            
            {$this->player2->name} : {$this->player2->coin}
            
            Победитель: {$this->winner()->name}
            
            Кол-во подбрасываний: {$this->flips}
            
EOT;

    }
}

$game = new Game(

    new Player('Joe', 100),
    new Player('Jane', 100)

);

$game->start();